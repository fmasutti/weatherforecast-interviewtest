//
//  CityWeatherCell.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation
import UIKit

final class CityWeatherCell: UITableViewCell {
    
    var city:CityModel!
    
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblContry: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    static public func cellIdentifier() -> String {
        return "CityWeatherCellIdentifier"
    }
    
    /// Method the load Cell content
    /// - Parameter cityParam: Its the object used to populate cell
    public func loadContent(cityParam: CityModel) {
        self.city = cityParam
        self.lblCityName.text = city.cityDBObj.name
        
        if let forecast = city.forecast.first, let temperature = forecast.main?.temp {
            self.lblTemp.text = "\(temperature.sfNone)º"
        }else {
            self.lblTemp.text = ""
        }
        self.lblContry.text = city.cityDBObj.country
        self.updatedBtnFavoriteStatus()
    }
    
    func updatedBtnFavoriteStatus() {
        
        if city.cityDBObj.favorite == false {
            self.imgIcon.alpha = 0.1
        }else {
            self.imgIcon.alpha = 1
        }
    }
    
}

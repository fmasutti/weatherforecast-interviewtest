//
//  CityWeatherFullInfoCell.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation
import UIKit

final class CityWeatherFullInfoCell: UITableViewCell {
    
    var city:CityModel!
    
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblContry: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var lblHumity: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    @IBOutlet weak var lblMain: UILabel!
    
    static public func cellIdentifier() -> String {
        return "CityWeatherFullInfoCellIdentifier"
    }
    
    /// Method the load Cell content
    /// - Parameter cityParam: Its the object used to populate cell
    public func loadContent(cityParam: CityModel) {
        self.imgIcon.alpha = 1
        self.imgIcon.layer.cornerRadius = 5
        self.city = cityParam
        self.lblCityName.text = city.cityDBObj.name
        if let forecast = city.forecast.first,
            let temperature = forecast.main?.temp,
            let humidity = forecast.main?.humidity,
            let pressure = forecast.main?.pressure,
            let icon = forecast.weather.first?.icon,
            let main = forecast.weather.first?.main {
            self.lblTemp.text = "\(temperature.sfNone)º"
            self.lblHumity.text = "\(Int(humidity))%"
            self.lblPressure.text = "\(Int(pressure)) hPa"
            self.lblMain.text = "\(main)"
            self.imgIcon.image = UIImage(named: icon)
        }else {
            self.lblTemp.text = ""
            self.lblPressure.text = ""
            self.lblHumity.text = ""
            self.imgIcon.image = nil
        }
        self.lblContry.text = city.cityDBObj.country
    }
}

//
//  MyCustonExtensions.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 05/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation


var numberFormatter = NumberFormatter()
extension Double {
    /// This method provide a formatd double in 2 decimal digits
    /// Return : Self Double as String and formated.
    var sf2Digits:String {
        get {
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            numberFormatter.maximumSignificantDigits = 2
            return numberFormatter.string(from: NSNumber(floatLiteral: self))!
        }
    }
    
    var sfNone:String {
        get {
            numberFormatter.numberStyle = NumberFormatter.Style.none
            return numberFormatter.string(from: NSNumber(floatLiteral: self))!
        }
    }
}

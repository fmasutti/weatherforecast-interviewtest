//
//  CityWeatherTableVC.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation
import SwiftSpinner
import UIKit

class CityWeatherTableVC: UITableViewController {
    
    var tblData = [CityModel]()
    var isCheckingForFavorites: Bool = true
    
    static public func segueIdentifier() -> String {
        return "cityWeatherTableVCSegue"
    }

    //MARK: View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateFilterContent()
    }
    
    //MARK: Tablew view delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tblData[indexPath.row].cityDBObj.favorite == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: CityWeatherFullInfoCell.cellIdentifier()) as! CityWeatherFullInfoCell
            cell.loadContent(cityParam: tblData[indexPath.row])
            if tblData[indexPath.row].forecast.first == nil {
                self.checkForWebContentForItem(indexPath: indexPath)
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CityWeatherCell.cellIdentifier()) as! CityWeatherCell
            cell.loadContent(cityParam: tblData[indexPath.row])
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tblData.underestimatedCount
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tblData[indexPath.row].cityDBObj.favorite {
            return 130
        }else {
            return 60
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tblData[indexPath.row].cityDBObj.favorite == false {
            let cell = tableView.dequeueReusableCell(withIdentifier: CityWeatherCell.cellIdentifier()) as! CityWeatherCell
            DatabaseManager.instance.update(isFavorite: true, cityId: tblData[indexPath.row].cityDBObj.cityId)
            self.tblData[indexPath.row].cityDBObj.favorite = true
            cell.loadContent(cityParam: tblData[indexPath.row])
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
    
    //Can remove just on favorites filter
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.isCheckingForFavorites == true {
            return true
        }else {
            return false
        }
        
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            DatabaseManager.instance.update(isFavorite: false, cityId: tblData[indexPath.row].cityDBObj.cityId)
            self.tblData.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
    }
    
    //MARK: Other methods
    
    //Method to check and download forecast info for indexPath.
    func checkForWebContentForItem(indexPath: IndexPath) {
        NetworkManager.instance.searchForecastByCityId(cityId: tblData[indexPath.row].cityDBObj.cityId, successHandler: { (response ) in
            self.tblData[indexPath.row].forecast = response.list
            DispatchQueue.main.async {
                self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                SwiftSpinner.hide()
            }
        }) { (response) in
            let alert = UIAlertController(title: "Ops", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.alert )
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Change filter
    
    //to change the filter on TableView.
    func changeFilter() {
        SwiftSpinner.show("Just a moment...")
        self.isCheckingForFavorites = !self.isCheckingForFavorites
        self.updateFilterContent()
    }
    
    //updade TableView content acording current filter "isCheckingForFavorites"
    private func updateFilterContent() {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            var tempInfo = [CityModel]()
            for cityObj in DatabaseManager.instance.selectAllCities(isFavorite: self.isCheckingForFavorites) {
                tempInfo.append(CityModel(city: cityObj))
            }
            DispatchQueue.main.async {
                SwiftSpinner.hide()
                self.tblData = tempInfo
                self.tableView.reloadData()
            }
        }
    }
}

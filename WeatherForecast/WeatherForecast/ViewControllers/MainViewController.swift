//
//  MainViewController.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 02/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var vEffectView: UIVisualEffectView!
    var weatherTVC: CityWeatherTableVC!
    
    //MARK: View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vEffectView.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Visual effect animation after Splash Screen.
        UIView.animate(withDuration: 2) {
            self.vEffectView.alpha = 0.8
        }
    }

    //MARK: Other Methods
    
    @IBAction func btnChangeFilter_Touched(_ sender: Any) {
        var rotate:CGFloat!
        if !self.weatherTVC.isCheckingForFavorites {
            rotate = CGFloat(Float(M_PI_2))
        }else {
            rotate = CGFloat(Float(M_PI_4))
        }
        UIView.animate(withDuration: 0.5) {
            self.btnFilter.transform = CGAffineTransform(rotationAngle: rotate);
        }
        self.weatherTVC.changeFilter()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == CityWeatherTableVC.segueIdentifier() {
            let connectContainerViewController = segue.destination as! CityWeatherTableVC
            weatherTVC = connectContainerViewController
        }
    }
}


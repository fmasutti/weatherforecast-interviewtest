//
//  NetworkManager.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation
import Alamofire

final class NetworkManager {
    
    private let kBase_URL = "http://api.openweathermap.org/data/2.5/"
    private let kAPPID = "82caf4adc49c36c6a110dffd813dcca3"
    private let kUnit = "metric" //imperial
    
    //Private init (Singleton class!)
    private init() { }
    
    //MARK: Shared Instance
    static let instance: NetworkManager = NetworkManager()
    
    //MARK: External Methods
    
    //Request methods
    public func searchWeaterByCityName(cityName:String) {
        let requestURL = "\(kBase_URL)weather?q=\(cityName)&APPID=\(kAPPID)"
        Alamofire.request(requestURL).responseJSON { response in
            #if DEBUG
                print(response.request! as Any)  // original URL request
                print(response.result)   // result of response serialization
            #endif
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
            }
        }
    }
    
    /// This method will search on the web api and return the update informations of a specifit city with id "x"
    /// - Parameter cityId: Its the id tha will
    /// - Parameter successHandler: is the returned block whem this method return success!
    /// - Parameter failHandler: is the returned block whem something went wron on the response/request
    public func searchForecastByCityId(cityId:Int32, successHandler: @escaping (_ response:WeatherResponse) -> Void, failHandler: @escaping (_ json:Any?) -> Void) {
        let requestURL = "\(kBase_URL)forecast?id=\(cityId)&APPID=\(kAPPID)&units=\(kUnit)"
        
        Alamofire.request(requestURL).responseJSON { response in
            #if DEBUG
                print(response.request as Any)  // original URL request
                print(response.result)   // result of response serialization
            #endif
            if let jsonObj = response.result.value as? [String:Any]{
                successHandler(WeatherResponse(json: jsonObj))
            }else {
                failHandler(response)
            }
        }
    }
}

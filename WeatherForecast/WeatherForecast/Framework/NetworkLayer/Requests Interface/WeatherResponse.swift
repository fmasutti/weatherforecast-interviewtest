//
//  WeatherResponseModel.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import AVFoundation

/// WeatherResponse is a structure that provide a interface response from a request of NetworkManager class.

struct WeatherResponse {
    
    var list:Array<ForecastModel>
    
    init(json: [String: Any]) {
        self.list = Array<ForecastModel>()
        if let jsonList = json["list"] as? NSArray {
            for jsonForecast in jsonList {
                self.list.append(ForecastModel(json: jsonForecast as! [String : Any]))
            }
        }
    }
}

//
//  CityDBObj.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 05/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation
import FMDB.FMResultSet

class CityDBObj {
    var cityId:Int32
    var name:String
    var lat:Double
    var lon:Double
    var country:String
    var favorite:Bool
    
    init(res:FMResultSet) {
        self.cityId = res.int(forColumn: "city_id")
        self.name = res.string(forColumn: "name")
        self.country = res.string(forColumn: "country")
        self.lat = res.double(forColumn: "lat")
        self.lon = res.double(forColumn: "lon")
        self.favorite = res.bool(forColumn: "favorite")
    }
    
}

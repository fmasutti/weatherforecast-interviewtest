//
//  Database.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 05/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation
import FMDB

final class DatabaseManager: FMDatabase {
    
    //MARK: Shared Instance
    static let instance: DatabaseManager = DatabaseManager()
    
    private override init() {
        super.init(path: DatabaseManager.getPath(dbName: "weather.sqlite"))
        self.traceExecution = true
        self.logsErrors = true
        self.open()
    }
    
    static func getPath(dbName: String) -> String {
        //Create dataBase patch
        let documentsURL = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dbPath = "\(documentsURL)/\(dbName)"
        
        //Check if the database from [NSBundle mainBundle] already has been copied
        if FileManager.default.fileExists(atPath: dbPath) == true {
            return dbPath
        }
        //Get the path to the database in the application package
        let dataBasePathFromApp = Bundle.main.resourcePath!.appending("/\(dbName)")
        do {
            try FileManager.default.copyItem(atPath: dataBasePathFromApp, toPath: dbPath)
        }catch {
            print(error.localizedDescription)
        }
        return dbPath
    }
    
    //This method will populate DB data from json.
    static func setupDataBase(dbPath: String) {
        //Load From json the cities info and append on db.
        if let path = Bundle.main.path(forResource: "city.list", ofType: "json") {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                let jsonStrings = data.components(separatedBy: .newlines)
                let db = FMDatabase(path: dbPath)
                db?.open()
                for jsonStr in jsonStrings {
                    if let data = jsonStr.data(using: .utf8) {
                        do {
                            //read line from file and convert to json and add to local DB.
                            let jsonObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                            if let cityId = jsonObj?["_id"] as? Int32,
                                let name = jsonObj?["name"] as? String,
                                let coord = jsonObj?["coord"] as? [String: Any],
                                let lon = coord["lon"] as? Double,
                                let lat = coord["lat"] as? Double,
                                let country = jsonObj?["country"] as? String {
                                
                                try! db!.executeUpdate("REPLACE INTO city (city_id, name, lat, lon, country, favorite) values (?, ?, ?, ?, ?, ?)", values: [cityId, name, lat, lon, country, false])
                            }
                            
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                }
                db?.close()
                print()
            } catch {
                print(error)
            }
        }
    }
    
    /// Select from DB of all cities with "Fovorite" flag true or false
    /// - Parameter isFavorite: flag used to make the select.
    func selectAllCities(isFavorite: Bool) -> [CityDBObj] {
        var listCities = [CityDBObj]()
        do {
            let query = "SELECT * FROM city WHERE favorite = ? ORDER BY name";
            let rs = try self.executeQuery(query, values: [isFavorite])
            while rs.next() {
                let city = CityDBObj(res: rs)
                listCities.append(city)
            }
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        return listCities
    }
    
    /// Update city withid the "favotire" flag.
    /// - Parameter cityId: id of city that will be updated
    /// - Parameter isFavorite: value to be seted.
    func update(isFavorite: Bool, cityId: Int32) {
        do {
            let query = "UPDATE city SET favorite = ? WHERE city_id = ?";
            try self.executeUpdate(query, values: [isFavorite, cityId])
        } catch {
            print("failed: \(error.localizedDescription)")
        }
    }
    
}

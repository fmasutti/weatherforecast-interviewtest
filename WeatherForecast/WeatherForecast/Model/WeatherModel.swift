//
//  WeatherModel.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation

/// WeatherModel is a interface of object to work with objects insted of dictionaries all the time.
/// In this structure you can found all the informations about the weather of a city.

struct WeatherModel {
    var description: String?
    var main: String?
    var icon: String?
    
    /// Init the structure with the json provided by the API.
    /// - Parameter json: Its the json provided from the "openweathermap" with the information to populate this model
    init(json: [String: Any]) {
        
        if let descriptionJson = json["description"] as? String {
            self.description = descriptionJson
        }else {
            print("WeatherModel - JSON Data corrupted - 'description' field")
        }
        
        if let mainJson = json["main"] as? String {
            self.main = mainJson
        }else {
            print("WeatherModel - JSON Data corrupted - 'main' field")
        }
        
        if let iconJson = json["icon"] as? String {
            self.icon = iconJson
        }else {
            print("WeatherModel - JSON Data corrupted - 'icon' field")
        }
        
    }
}

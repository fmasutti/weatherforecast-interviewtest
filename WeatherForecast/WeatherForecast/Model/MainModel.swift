//
//  MainModel.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 05/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation

/// MainModel is a interface of object to work with objects insted of dictionaries all the time.
/// In this structure you can find like a resume for the weather for the city.

struct MainModel {
    var grnd_level: Double?
    var humidity: Double?
    var pressure: Double?
    var temp: Double?
    var temp_max: Double?
    var temp_min: Double?
    
    /// Init the structure with the json provided by the API.
    /// - Parameter json: Its the json provided from the "openweathermap" with the information to populate this model
    
    init(json: [String: Any]) {
        
        if let grnd_levelJson = json["grnd_level"] as? Double {
            self.grnd_level = grnd_levelJson
        }else {
            print("MainModel - JSON Data corrupted - 'grnd_level' field")
        }
        
        if let humidityJson = json["humidity"] as? Double {
            self.humidity = humidityJson
        }else {
            print("MainModel - JSON Data corrupted - 'main' field")
        }
        
        if let pressureJson = json["pressure"] as? Double {
            self.pressure = pressureJson
        }else {
            print("MainModel - JSON Data corrupted - 'pressure' field")
        }
        
        if let tempJson = json["temp"] as? Double {
            self.temp = tempJson
        }else {
            print("MainModel - JSON Data corrupted - 'temp' field")
        }
        
        if let temp_maxJson = json["temp_max"] as? Double {
            self.temp_max = temp_maxJson
        }else {
            print("MainModel - JSON Data corrupted - 'temp_max' field")
        }
        
        if let temp_minJson = json["temp_min"] as? Double {
            self.temp_min = temp_minJson
        }else {
            print("MainModel - JSON Data corrupted - 'temp_min' field")
        }
        
    }
}

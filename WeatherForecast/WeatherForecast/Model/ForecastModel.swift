//
//  ForecastModel.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation

/// ForecastModel is a interface of object to work with objects insted of dictionaries all the time.
/// In this structure you can found all the informations about the forecast of a city.
struct ForecastModel {
    var dt: Date?
    var dt_txt: String?
    var wind:WindModel?
    var weather = [WeatherModel]()
    var main:MainModel?
    
    /// Init the structure withthe json provided by the API.
    /// - Parameter json: Its the json provided from the "openweathermap" with the information to populate this model
    init(json: [String: Any]) {
        if let dtJson = json["dt"] as? TimeInterval {
            self.dt = Date(timeIntervalSinceNow: dtJson)
        }else {
            print("ForecastModel - JSON Data corrupted - 'dt' field")
        }
        
        if let dt_txtJson = json["dt_txt"] as? String {
            self.dt_txt = dt_txtJson
        }else {
            print("ForecastModel - JSON Data corrupted - 'dt_txt' field")
        }
        
        if let weatherJsonList = json["weather"] as? [[String:Any]] {
            for weatherJson in weatherJsonList {
                self.weather.append(WeatherModel(json: weatherJson))
            }
        }else {
            print("ForecastModel - JSON Data corrupted - 'wind' field")
        }
        
        if let windJson = json["wind"] as? [String:Any] {
            self.wind = WindModel(json: windJson)
        }else {
            print("ForecastModel - JSON Data corrupted - 'wind' field")
        }
        
        if let windJson = json["main"] as? [String:Any] {
            self.main = MainModel(json: windJson)
        }else {
            print("ForecastModel - JSON Data corrupted - 'main' field")
        }
    }
    
}

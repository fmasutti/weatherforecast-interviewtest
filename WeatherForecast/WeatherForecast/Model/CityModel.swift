//
//  CityModel.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

/// CityModel is a interface that provide all the informations about a city.

struct CityModel {
    
    
    var cityDBObj:CityDBObj
    var forecast:[ForecastModel]
    
    init(city:CityDBObj) {
        self.cityDBObj = city
        self.forecast = [ForecastModel]()
    }
    
}

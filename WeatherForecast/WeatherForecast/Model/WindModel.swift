//
//  WindModel.swift
//  WeatherForecast
//
//  Created by Frantiesco Masutti on 03/03/17.
//  Copyright © 2017 Frantiesco Masutti. All rights reserved.
//

import Foundation

/// WindModel is a interface of object to work with objects insted of dictionaries all the time.
/// In this structure you can found all the informations about the wind of a city.

struct WindModel {
    let deg: Double
    let speed: Double
    
    /// Init the structure with the json provided by the API.
    /// - Parameter json: Its the json provided from the "openweathermap" with the information to populate this model
    
    init(json: [String: Any]) {
        self.deg = (json["deg"] as? Double)!
        self.speed = (json["speed"] as? Double)!
    }
}
